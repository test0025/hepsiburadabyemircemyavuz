Feature: user should be able to add any item to shopping cart
  @e2e
  Scenario: Go to the search results web page
    Given  The user is on the search results
    When   The user clicks on first item
    Then   The user should be able to see page of the item
    And    The user should be able to add the item to shopping cart