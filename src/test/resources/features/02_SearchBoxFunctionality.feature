Feature: user should be able to type and search an item on hepsiburada searchbox
@e2e
  Scenario: Go to the main page of the website
    When   The user clicks on search box and type any item
    Then   The user should be able to click search button
    And    The user should see the related search results