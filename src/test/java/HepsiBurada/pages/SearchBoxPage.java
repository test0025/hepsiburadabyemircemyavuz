package HepsiBurada.pages;

import HepsiBurada.utilities.Driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchBoxPage extends BasePage {
    public SearchBoxPage(){
        PageFactory.initElements(Driver.get(), this);
    }
    @FindBy(xpath = "/html[1]/body[1]/div[3]/main[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/section[1]/div[1]/div[2]/div[1]/div[1]/div[1]/h1[1]")
    public WebElement itemName;

}
