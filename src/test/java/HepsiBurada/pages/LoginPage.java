package HepsiBurada.pages;

import HepsiBurada.utilities.Driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class LoginPage extends  BasePage {
    public LoginPage(){
        PageFactory.initElements(Driver.get(), this);
    }

    @FindBy(css ="#txtUserName")
    public WebElement ePostaAdresi;
    @FindBy(css = "#txtPassword")
    public WebElement sifre;
    @FindBy(css = "#btnLogin")
    public WebElement GirisYapBtn;



}
