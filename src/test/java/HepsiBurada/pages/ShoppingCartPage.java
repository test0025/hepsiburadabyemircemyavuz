package HepsiBurada.pages;

import HepsiBurada.utilities.Driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ShoppingCartPage {
    public ShoppingCartPage(){ PageFactory.initElements(Driver.get(), this); }
    @FindBy(xpath = "/html[1]/body[1]/div[3]/main[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/section[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[1]/div[1]/a[1]/div[1]/figure[1]/div[1]")
    public WebElement clickFirstItem;
    @FindBy(xpath = "//button[@id='addToCart']")
    public WebElement addToShoppingCartBtn;
    @FindBy(xpath = "//h1[@id='product-name']")
    public WebElement itemsTitle;

}
