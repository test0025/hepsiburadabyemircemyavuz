package HepsiBurada.pages;

import HepsiBurada.utilities.Driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public abstract class BasePage {
    Actions actions=new Actions(Driver.get());
    public BasePage() {
        PageFactory.initElements(Driver.get(), this);
    }
    @FindBy(xpath = "//div[@id='myAccount']")
    public WebElement KullaniciArayuzu;
    @FindBy(css = "#login")
    public WebElement GirisSayfasi;
    @FindBy(xpath = "//body/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[1]/ul[1]/li[3]/a[1]")
    public WebElement Siparislerim;
    @FindBy(xpath = "//*[@id=\"myAccount\"]/span/a/span[1]")
    public WebElement Hesabim;
    @FindBy(xpath = "//body/div[@id='container']/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[1]/ul[1]/li[5]/a[1]")
    public WebElement KullaniciHesabi;
    @FindBy(xpath = "//body/div[@id='container']/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/input[1]")
    public WebElement AramaKutusu;
    @FindBy(xpath = "//div[contains(text(),'ARA')]")
    public WebElement AraButton;
    @FindBy(xpath = "//span[@id='shoppingCart']")
    public WebElement SepetimButton;

}
