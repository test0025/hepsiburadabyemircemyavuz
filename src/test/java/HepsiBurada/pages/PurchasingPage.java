package HepsiBurada.pages;

import HepsiBurada.utilities.Driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PurchasingPage extends BasePage {
    public PurchasingPage(){
        PageFactory.initElements(Driver.get(), this);
    }

    @FindBy(xpath = "//body/div[@id='app']/div[1]/div[1]/div[2]/div[1]/div[2]/section[1]/section[1]/ul[1]/li[1]/div[1]/div[1]/div[2]/div[2]")
    public WebElement theItemTitle;
    @FindBy(xpath = "//button[@id='continue_step_btn']")
    public WebElement alisverisiTamamla;
    @FindBy(xpath = "//*[@id=\"app\"]/div/div/div[2]/div/div[3]/div/div/div[1]/div[1]/span[2]/div/div")
    public WebElement priceOfItem;
    @FindBy (xpath = "//*[@id=\"app\"]/div/div/div[1]/div/div[2]/div/div/div[1]/div[1]/span[2]/div/div")
    public WebElement secondPriceOfItem;
}
