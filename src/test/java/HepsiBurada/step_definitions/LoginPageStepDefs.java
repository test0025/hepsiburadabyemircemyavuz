package HepsiBurada.step_definitions;
import HepsiBurada.pages.LoginPage;
import HepsiBurada.utilities.BrowserUtilities;
import HepsiBurada.utilities.ConfigurationReader;
import HepsiBurada.utilities.Driver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.en_old.Ac;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

public class LoginPageStepDefs {


    @Given("The user is on the login page")
    public void the_user_is_on_the_login_page() {
        String url= ConfigurationReader.get("url");
        Driver.get().get(url);
        BrowserUtilities.waitForClickablility(new LoginPage().KullaniciArayuzu,10);
        new LoginPage().KullaniciArayuzu.click();
        BrowserUtilities.waitForClickablility(new LoginPage().GirisSayfasi,10);
        new LoginPage().GirisSayfasi.click();


    }


    @When("The user enters valid credentials and click login button")
    public void the_user_enters_valid_credentials_and_click_login_button() {
        String username=ConfigurationReader.get("username");
        String password=ConfigurationReader.get("password");
        BrowserUtilities.waitForClickablility(new LoginPage().ePostaAdresi,10);
        new LoginPage().ePostaAdresi.sendKeys(username);
        new LoginPage().sifre.sendKeys(password);
        new LoginPage().GirisYapBtn.click();




    }
    @Then("The user should see the main page of the website")
    public void the_user_should_see_the_main_page_of_the_website()  {
        BrowserUtilities.waitForVisibility(new LoginPage().Hesabim, 10);
        String expectedTitle="Türkiye'nin En Büyük Online Alışveriş Sitesi Hepsiburada.com";
        String actualTitle= Driver.get().getTitle();
        Assert.assertEquals(expectedTitle,actualTitle);


    }





}
