package HepsiBurada.step_definitions;

import HepsiBurada.pages.SearchBoxPage;
import HepsiBurada.utilities.BrowserUtilities;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class SearchBoxStepDefs {

    @When("The user clicks on search box and type any item")
    public void the_user_clicks_on_search_box_and_type_any_item() {
    BrowserUtilities.waitForClickablility(new SearchBoxPage().AramaKutusu,10);
    new SearchBoxPage().AramaKutusu.click();
    new SearchBoxPage().AramaKutusu.sendKeys("Samsung");

    }
    @Then("The user should be able to click search button")
    public void the_user_should_be_able_to_click_search_button() {
    new SearchBoxPage().AraButton.click();

    }
    @Then("The user should see the related search results")
    public void the_user_should_see_the_related_search_results() {
    BrowserUtilities.waitForVisibility(new SearchBoxPage().itemName,10);
    String expectedResult="Samsung";
    String actualResult= new SearchBoxPage().itemName.getText();
    Assert.assertEquals(expectedResult,actualResult);

    }
}
