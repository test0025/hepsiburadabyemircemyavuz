package HepsiBurada.step_definitions;

import HepsiBurada.pages.PurchasingPage;
import HepsiBurada.utilities.BrowserUtilities;
import HepsiBurada.utilities.Driver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class PurchasingStepDefs  {
    String itemPrice="";

    @Given("The user is on the shopping cart page")
    public void the_user_is_on_the_shopping_cart_page() {
        BrowserUtilities.waitForClickablility(new PurchasingPage().SepetimButton,10);
        new PurchasingPage().SepetimButton.click();

        BrowserUtilities.waitForVisibility(new PurchasingPage().priceOfItem,10);
        itemPrice=new PurchasingPage().priceOfItem.getText();


    }

    @Then("The user should be able to see the first item is in the shopping cart")
    public void the_user_should_be_able_to_see_the_first_item_is_in_the_shopping_cart() {
        BrowserUtilities.waitForVisibility(new PurchasingPage().theItemTitle,10);
        String expectedTitle=new PurchasingPage().theItemTitle.getText();
        String actualTitle=new ShoppingCartStepDefs().actualtitle;
        if (expectedTitle.contains(actualTitle)){
            System.out.println("Items are matching");
        }



    }
    @When("The user clicks Alışverişi tamamla button")
    public void the_user_clicks_Alışverişi_tamamla_button() {
        BrowserUtilities.waitForClickablility(new PurchasingPage().alisverisiTamamla,10);
        new PurchasingPage().alisverisiTamamla.click();


    }
    @Then("The user should be able to see Ödenecek Tutar")
    public void the_user_should_be_able_to_see_Ödenecek_Tutar() {
        BrowserUtilities.waitForVisibility(new PurchasingPage().secondPriceOfItem,10);
        String expectedPrice=new PurchasingPage().secondPriceOfItem.getText();
        String actualPrice=itemPrice;
        if (expectedPrice.contains(actualPrice))
        {
            System.out.println("Prices are matching");
        }
       Driver.closeDriver();
    }

}
