package HepsiBurada.step_definitions;

import HepsiBurada.pages.ShoppingCartPage;
import HepsiBurada.utilities.BrowserUtilities;
import HepsiBurada.utilities.Driver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ShoppingCartStepDefs {
    String actualtitle="";

    @Given("The user is on the search results")
    public void the_user_is_on_the_search_results() {

    }


    @When("The user clicks on first item")
    public void the_user_clicks_on_any_item() {
        BrowserUtilities.waitForClickablility(new ShoppingCartPage().clickFirstItem,10);
        new ShoppingCartPage().clickFirstItem.click();



    }
    @Then("The user should be able to see page of the item")
    public void the_user_should_be_able_to_see_page_of_the_item()  {
        BrowserUtilities.waitForVisibility(new ShoppingCartPage().itemsTitle,10);
        String expectedTitle=new ShoppingCartPage().itemsTitle.getText();
        String actualTitle=Driver.get().getTitle();

    if (expectedTitle.contains(actualTitle));
        {
            System.out.println("Titles are matching");
        }


    }
    @Then("The user should be able to add the item to shopping cart")
    public void the_user_should_be_able_to_add_the_item_to_shopping_cart() {
        BrowserUtilities.waitForClickablility(new ShoppingCartPage().addToShoppingCartBtn,10);
        new ShoppingCartPage().addToShoppingCartBtn.click();

    }
}
