$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/01_LoginFunctionality.feature");
formatter.feature({
  "name": "user should be able to log in hepsiburada succesfully",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Go to the login page of the website",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@e2e"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "The user is on the login page",
  "keyword": "Given "
});
formatter.match({
  "location": "HepsiBurada.step_definitions.LoginPageStepDefs.the_user_is_on_the_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user enters valid credentials and click login button",
  "keyword": "When "
});
formatter.match({
  "location": "HepsiBurada.step_definitions.LoginPageStepDefs.the_user_enters_valid_credentials_and_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user should see the main page of the website",
  "keyword": "Then "
});
formatter.match({
  "location": "HepsiBurada.step_definitions.LoginPageStepDefs.the_user_should_see_the_main_page_of_the_website()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/02_SearchBoxFunctionality.feature");
formatter.feature({
  "name": "user should be able to type and search an item on hepsiburada searchbox",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Go to the main page of the website",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@e2e"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "The user clicks on search box and type any item",
  "keyword": "When "
});
formatter.match({
  "location": "HepsiBurada.step_definitions.SearchBoxStepDefs.the_user_clicks_on_search_box_and_type_any_item()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user should be able to click search button",
  "keyword": "Then "
});
formatter.match({
  "location": "HepsiBurada.step_definitions.SearchBoxStepDefs.the_user_should_be_able_to_click_search_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user should see the related search results",
  "keyword": "And "
});
formatter.match({
  "location": "HepsiBurada.step_definitions.SearchBoxStepDefs.the_user_should_see_the_related_search_results()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/03_AddingAnItemtoShoppingCart.feature");
formatter.feature({
  "name": "user should be able to add any item to shopping cart",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Go to the search results web page",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@e2e"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "The user is on the search results",
  "keyword": "Given "
});
formatter.match({
  "location": "HepsiBurada.step_definitions.ShoppingCartStepDefs.the_user_is_on_the_search_results()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user clicks on first item",
  "keyword": "When "
});
formatter.match({
  "location": "HepsiBurada.step_definitions.ShoppingCartStepDefs.the_user_clicks_on_any_item()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user should be able to see page of the item",
  "keyword": "Then "
});
formatter.match({
  "location": "HepsiBurada.step_definitions.ShoppingCartStepDefs.the_user_should_be_able_to_see_page_of_the_item()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user should be able to add the item to shopping cart",
  "keyword": "And "
});
formatter.match({
  "location": "HepsiBurada.step_definitions.ShoppingCartStepDefs.the_user_should_be_able_to_add_the_item_to_shopping_cart()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/04_Purchasing.feature");
formatter.feature({
  "name": "user should be able to purchase the item in the shopping cart",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Go to the shopping cart",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@e2e"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "The user is on the shopping cart page",
  "keyword": "Given "
});
formatter.match({
  "location": "HepsiBurada.step_definitions.PurchasingStepDefs.the_user_is_on_the_shopping_cart_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user should be able to see the first item is in the shopping cart",
  "keyword": "Then "
});
formatter.match({
  "location": "HepsiBurada.step_definitions.PurchasingStepDefs.the_user_should_be_able_to_see_the_first_item_is_in_the_shopping_cart()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user clicks Alışverişi tamamla button",
  "keyword": "When "
});
formatter.match({
  "location": "HepsiBurada.step_definitions.PurchasingStepDefs.the_user_clicks_Alışverişi_tamamla_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user should be able to see Ödenecek Tutar",
  "keyword": "Then "
});
formatter.match({
  "location": "HepsiBurada.step_definitions.PurchasingStepDefs.the_user_should_be_able_to_see_Ödenecek_Tutar()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});