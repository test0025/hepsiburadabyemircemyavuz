Feature: user should be able to purchase the item in the shopping cart
  @e2e
  Scenario: Go to the shopping cart
    Given  The user is on the shopping cart page
    Then   The user should be able to see the first item is in the shopping cart
    When   The user clicks Alışverişi tamamla button
    Then   The user should be able to see Ödenecek Tutar
